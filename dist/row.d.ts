import Dataset from './dataset';
export interface IData {
    [s: string]: any;
    dv: number;
    id: number;
}
interface IDelete {
    error?: string;
    success: boolean;
}
export default class Row {
    private _id;
    private _host;
    constructor(host: Dataset, id: number);
    data(): Promise<IData>;
    id(): number;
    update(data: {
        [s: string]: any;
    }): Promise<IData>;
    delete(): Promise<IDelete>;
}
export {};
