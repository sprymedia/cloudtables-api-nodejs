/*! CloudTables API client
 * © SpryMedia Ltd - MIT licensed
 */
import Dataset from './dataset';
export interface IDatasetInfo {
    deleteCount: number;
    id: string;
    insertCount: number;
    lastData: number;
    name: number;
    readCount: number;
    rowCount: number;
    updateCount: number;
}
export interface IDatasetData {
    columns: Array<{
        id: number;
        linkTable: string | null;
        name: string;
    }>;
    data: Array<{
        [s: number]: any;
        dv: number;
        id: number;
    }>;
}
interface ICondition {
    id?: string;
    name?: string;
    op?: '=' | '!=' | '<' | '<=' | '>=' | '>';
    set?: boolean;
    setValue?: string;
    value: string | number;
}
interface IConditionSimple {
    [key: string]: number | string;
}
export declare type IStyleFramework = 'd' | 'auto' | 'bs' | 'bs4' | 'dt' | 'zf' | 'ju' | 'se' | 'no';
export interface IOptions {
    /** Your unique identifier for the user */
    clientId?: string;
    /** Addition information that you can use to identify the user (e.g. an access type or source) */
    clientMeta?: string;
    /** Name / label to give the user in the CloudTables configuration UI */
    clientName?: string;
    /** Conditions to apply to the data to fetch */
    conditions?: ICondition[] | IConditionSimple;
    /** Domain */
    domain?: string;
    /** Authentication duration - seconds */
    duration?: number;
    /** Single role (takes priority over `roles` if both used) */
    role?: string;
    /** Array of roles (union'ed with the roles for the API key) */
    roles?: string[];
    /** Allow a self signed certificate to be accepted or not (still https) */
    secure?: boolean;
    /** Use https or not */
    ssl?: boolean;
}
export default class CloudTablesApi {
    private _subdomain;
    private _key;
    private _options;
    private _accessKey;
    /**
     * Access a SELF-HOSTED CloudTables API
     * @param key API access key
     * @param options Configuration options
     */
    constructor(key: string, options: IOptions | null);
    /**
     * Access a CLOUD CloudTables API
     * @param subdomain Sub-domain name
     * @param key API access key
     * @param options Configuration options
     */
    constructor(subdomain: string, key: string, options: IOptions | null);
    /**
     * Get the data and columns for a given dataset
     *
     * @param dataset ID (UUID) of the dataset to be shown
     * @deprecated Use `dataset().data()`
     */
    data(dataset: string): Promise<IDatasetData>;
    /**
     * Get summary information about the available datasets
     */
    datasets(): Promise<IDatasetInfo[]>;
    /**
     * Manipulate a specific data set
     * @param id Data set id
     * @returns Data set instance
     */
    dataset(id: string): Dataset;
    /**
     * Get an access token. Note that this value is cached - a single token per instance is
     * all that is required.
     *
     * @returns Access token
     */
    token(): Promise<string>;
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * @param token Secure access token (from `.token()`)
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     * @deprecated Use `dataset().scriptTag()`
     */
    scriptTag(token: string, datasetId: string, style?: IStyleFramework): string;
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
     * requiring it to be async.
     *
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     * @deprecated Use `dataset().scriptTagAsync()`
     */
    scriptTagAsync(datasetId: string, style?: IStyleFramework): Promise<string>;
}
export {};
