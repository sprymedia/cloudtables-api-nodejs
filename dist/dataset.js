"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var row_1 = require("./row");
var Dataset = /** @class */ (function () {
    function Dataset(host, id) {
        this._host = host;
        this._id = id;
    }
    /**
     * Get the data for a given dataset
     */
    Dataset.prototype.data = function () {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._host.request('/api/1/dataset/' + this._id, 'GET')];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    Dataset.prototype.id = function () {
        return this._id;
    };
    /**
     * Insert a new row into the data set
     * @param data Data to insert as a new row
     * @returns Newly inserted row's data
     */
    Dataset.prototype.insert = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._host.request("/api/1/dataset/" + this.id(), 'POST', data)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    /**
     * Manipulate a row
     * @param id Row ID
     * @returns Row instance
     */
    Dataset.prototype.row = function (id) {
        var r = new row_1.default(this, id);
        return r;
    };
    /** Get the data structure for the data set */
    Dataset.prototype.schema = function () {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._host.request("/api/1/dataset/" + this._id + "/schema", 'GET')];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * @param token Secure access token (from `.token()`)
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     */
    Dataset.prototype.scriptTag = function (token, style) {
        if (style === void 0) { style = 'd'; }
        var api = this._host;
        var opts = api.options();
        var protocol = opts.ssl === false
            ? 'http://'
            : 'https://';
        // Without a subdomain we need to add a leading `/io` for self hosting CT
        return api.subdomain()
            ? "<script src=\"" + protocol + api.subdomain() + "." + opts.domain +
                ("/loader/" + this._id + "/table/" + style + "\" data-token=\"" + token + "\"></script>")
            : "<script src=\"" + protocol + opts.domain +
                ("/io/loader/" + this._id + "/table/" + style + "\" data-token=\"" + token + "\"></script>");
    };
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
     * requiring it to be async.
     *
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     */
    Dataset.prototype.scriptTagAsync = function (style) {
        if (style === void 0) { style = 'd'; }
        return __awaiter(this, void 0, void 0, function () {
            var api, token;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        api = this._host;
                        return [4 /*yield*/, api.token()];
                    case 1:
                        token = _a.sent();
                        return [2 /*return*/, this.scriptTag(token, style)];
                }
            });
        });
    };
    /** @internal */
    Dataset.prototype.api = function () {
        return this._host;
    };
    return Dataset;
}());
exports.default = Dataset;
