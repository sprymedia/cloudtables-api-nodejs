import CloudTablesApi, { IStyleFramework } from './CloudTablesApi';
import Row, { IData as IRowData } from './row';
export interface IData {
    data: IRowData[];
}
export interface INewRowData {
    [s: string]: any;
}
interface IApiColumn {
    id: string;
    link: string | null;
    name: string;
}
interface IPoints {
    computed: Array<{
        id: string;
        name: string;
    }>;
    datapoints: Array<{
        id: string;
        name: string;
    }>;
    links: Array<IPoints | {
        id: string;
        name: string;
        target: {
            id: string;
        };
    }>;
}
interface ISchema extends IPoints {
    description: string;
    name: string;
    table: IApiColumn[];
}
export default class Dataset {
    private _id;
    private _host;
    constructor(host: CloudTablesApi, id: string);
    /**
     * Get the data for a given dataset
     */
    data(): Promise<IData>;
    id(): string;
    /**
     * Insert a new row into the data set
     * @param data Data to insert as a new row
     * @returns Newly inserted row's data
     */
    insert(data: INewRowData): Promise<IData>;
    /**
     * Manipulate a row
     * @param id Row ID
     * @returns Row instance
     */
    row(id: number): Row;
    /** Get the data structure for the data set */
    schema(): Promise<ISchema>;
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * @param token Secure access token (from `.token()`)
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     */
    scriptTag(token: string, style?: IStyleFramework): string;
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
     * requiring it to be async.
     *
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     */
    scriptTagAsync(style?: IStyleFramework): Promise<string>;
}
export {};
