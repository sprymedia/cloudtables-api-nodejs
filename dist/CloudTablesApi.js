"use strict";
/*! CloudTables API client
 * © SpryMedia Ltd - MIT licensed
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var https = require("https");
var stringify = require("qs-stringify");
var dataset_1 = require("./dataset");
var CloudTablesApi = /** @class */ (function () {
    function CloudTablesApi(a, b, c) {
        if (b === void 0) { b = null; }
        if (c === void 0) { c = null; }
        this._accessKey = null;
        var key;
        var options;
        ;
        if (typeof a === 'string' && typeof b === 'string') {
            // Cloud
            this._subdomain = a;
            key = b;
            options = c;
        }
        else {
            // Self-hosted
            this._subdomain = '';
            key = a;
            options = b;
        }
        this._key = key;
        this._options = __assign({ clientId: undefined, clientMeta: undefined, clientName: undefined, conditions: undefined, domain: 'cloudtables.io', duration: undefined, role: undefined, roles: [], secure: true }, options);
    }
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Public methods
     */
    /**
     * Get the data and columns for a given dataset
     *
     * @param dataset ID (UUID) of the dataset to be shown
     * @deprecated Use `dataset().data()`
     */
    CloudTablesApi.prototype.data = function (dataset) {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.request('/api/1/dataset/' + dataset + '/data', 'GET')];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json];
                }
            });
        });
    };
    /**
     * Get summary information about the available datasets
     */
    CloudTablesApi.prototype.datasets = function () {
        return __awaiter(this, void 0, void 0, function () {
            var json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.request('/api/1/datasets', 'GET')];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json.datasets];
                }
            });
        });
    };
    /**
     * Manipulate a specific data set
     * @param id Data set id
     * @returns Data set instance
     */
    CloudTablesApi.prototype.dataset = function (id) {
        var ds = new dataset_1.default(this, id);
        return ds;
    };
    /**
     * Get an access token. Note that this value is cached - a single token per instance is
     * all that is required.
     *
     * @returns Access token
     */
    CloudTablesApi.prototype.token = function () {
        return __awaiter(this, void 0, void 0, function () {
            var opts, data, json;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this._accessKey !== null) {
                            return [2 /*return*/, this._accessKey];
                        }
                        opts = this._options;
                        data = {};
                        if (opts.duration !== undefined) {
                            data.duration = opts.duration;
                        }
                        if (opts.clientId !== undefined) {
                            data.clientId = opts.clientId;
                        }
                        if (opts.clientMeta !== undefined) {
                            data.clientMeta = opts.clientMeta;
                        }
                        if (opts.clientName !== undefined) {
                            data.clientName = opts.clientName;
                        }
                        return [4 /*yield*/, this.request('/api/1/access', 'POST', data)];
                    case 1:
                        json = _a.sent();
                        return [2 /*return*/, json.token];
                }
            });
        });
    };
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * @param token Secure access token (from `.token()`)
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     * @deprecated Use `dataset().scriptTag()`
     */
    CloudTablesApi.prototype.scriptTag = function (token, datasetId, style) {
        if (style === void 0) { style = 'd'; }
        return this.dataset(datasetId).scriptTag(token, style);
    };
    /**
     * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
     * where you want the table to appear in your HTML.
     *
     * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
     * requiring it to be async.
     *
     * @param datasetId ID (UUID) of the dataset to be shown
     * @param style The styling framework to use for this table
     * @returns `<script>` tag to use.
     * @deprecated Use `dataset().scriptTagAsync()`
     */
    CloudTablesApi.prototype.scriptTagAsync = function (datasetId, style) {
        if (style === void 0) { style = 'd'; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.dataset(datasetId).scriptTagAsync(style)];
            });
        });
    };
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Internal methods for use by the other CT classes
     */
    /** @internal */
    CloudTablesApi.prototype.options = function () {
        return this._options;
    };
    /** @internal */
    CloudTablesApi.prototype.subdomain = function () {
        return this._subdomain;
    };
    /**
     * Make a request to get data
     * @internal
     */
    CloudTablesApi.prototype.request = function (path, type, data) {
        if (data === void 0) { data = {}; }
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var opts, postData, options, host, protocol, promise;
            return __generator(this, function (_b) {
                opts = this._options;
                // Add common data
                data.key = this._key;
                if ((_a = opts.roles) === null || _a === void 0 ? void 0 : _a.length) {
                    data.roles = opts.roles;
                }
                if (opts.role) {
                    data.roles = [opts.role];
                }
                if (opts.conditions !== undefined) {
                    data.conditions = opts.conditions;
                }
                postData = stringify(data);
                options = {};
                // For self hosting, there is no subdomain and we need a /io prefix for the API server
                if (!this._subdomain) {
                    host = this._options.domain;
                    path = '/io' + path;
                }
                else {
                    host = this._subdomain + '.' + this._options.domain;
                }
                if (type === 'POST' || type === 'PUT') {
                    options = {
                        headers: {
                            'Content-Length': postData.length,
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        hostname: host,
                        method: type,
                        path: path,
                    };
                }
                else {
                    options = {
                        hostname: host,
                        method: type,
                        path: path + '?' + postData
                    };
                }
                // Allow self signed certificates
                if (!opts.secure) {
                    options.rejectUnauthorized = false;
                    options.agent = false;
                }
                protocol = opts.ssl === false
                    ? http
                    : https;
                promise = new Promise(function (resolve, reject) {
                    var data = [];
                    var req = protocol.request(options, function (res) {
                        res.on('data', function (chunk) {
                            data.push(chunk);
                        });
                        res.on('end', function () {
                            try {
                                var str = Buffer.concat(data).toString();
                                var json = JSON.parse(str);
                                if (json.errors) {
                                    console.error('The response from the server indicated errors: ' + data);
                                    reject(json.errors);
                                }
                                else {
                                    resolve(json);
                                }
                            }
                            catch (e) {
                                console.error('JSON return from the server was not JSON: ' + data);
                                reject('JSON encoding error');
                            }
                        });
                    });
                    req.on('error', function (e) {
                        reject(e);
                    });
                    if (type === 'POST' || type === 'PUT') {
                        req.write(postData);
                    }
                    req.end();
                });
                return [2 /*return*/, promise];
            });
        });
    };
    return CloudTablesApi;
}());
exports.default = CloudTablesApi;
