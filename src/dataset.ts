
import CloudTablesApi, {IStyleFramework} from './CloudTablesApi';
import Row, {IData as IRowData} from './row';

export interface IData {
	data: IRowData[];
}

export interface INewRowData {
	[s: string]: any;
}

interface IApiColumn {
	id: string;
	link: string | null;
	name: string;
}

interface IPoints {
	computed: Array<{
		id: string;
		name: string;
	}>;
	datapoints: Array<{
		id: string;
		name: string;
	}>;
	links: Array<IPoints | {
		id: string;
		name: string;
		target: {
			id: string;
		}
	}>;
}

interface ISchema extends IPoints {
	description: string;
	name: string;
	table: IApiColumn[];
}

export default class Dataset {
	private _id: string;
	private _host: CloudTablesApi;

	public constructor (host: CloudTablesApi, id: string) {
		this._host = host;
		this._id = id;
	}

	/**
	 * Get the data for a given dataset
	 */
	public async data() {
		let json = await this._host.request<IData>('/api/1/dataset/' + this._id, 'GET');

		return json;
	}

	public id() {
		return this._id;
	}

	/**
	 * Insert a new row into the data set
	 * @param data Data to insert as a new row
	 * @returns Newly inserted row's data
	 */
	public async insert(data: INewRowData) {
		let json = await this._host.request<IData>(
			`/api/1/dataset/${this.id()}`,
			'POST',
			data
		);

		return json;
	}

	/**
	 * Manipulate a row
	 * @param id Row ID
	 * @returns Row instance
	 */
	public row(id: number) {
		let r = new Row(this, id);
	
		return r;
	}

	/** Get the data structure for the data set */
	public async schema() {
		let json = await this._host.request<ISchema>(
			`/api/1/dataset/${this._id}/schema`,
			'GET'
		);

		return json;
	}

	/**
	 * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
	 * where you want the table to appear in your HTML.
	 *
	 * @param token Secure access token (from `.token()`)
	 * @param datasetId ID (UUID) of the dataset to be shown
	 * @param style The styling framework to use for this table
	 * @returns `<script>` tag to use.
	 */
	public scriptTag(token: string, style: IStyleFramework = 'd') {
		let api = this._host;
		let opts = api.options();
		let protocol = opts.ssl === false
			? 'http://'
			: 'https://';

		// Without a subdomain we need to add a leading `/io` for self hosting CT
		return api.subdomain()
			? `<script src="${protocol}${api.subdomain()}.${opts.domain}` +
				`/loader/${this._id}/table/${style}" data-token="${token}"></script>`
			: `<script src="${protocol}${opts.domain}` +
				`/io/loader/${this._id}/table/${style}" data-token="${token}"></script>`;
	}

	/**
	 * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
	 * where you want the table to appear in your HTML.
	 *
	 * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
	 * requiring it to be async.
	 *
	 * @param datasetId ID (UUID) of the dataset to be shown
	 * @param style The styling framework to use for this table
	 * @returns `<script>` tag to use.
	 */
	public async scriptTagAsync(style: IStyleFramework = 'd') {
		let api = this._host;
		let token = await api.token();

		return this.scriptTag(token, style);
	}

	/** @internal */
	public api() {
		return this._host;
	}
}
