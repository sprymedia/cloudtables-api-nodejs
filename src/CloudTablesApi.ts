/*! CloudTables API client
 * © SpryMedia Ltd - MIT licensed
 */

import * as http from 'http';
import * as https from 'https';
import stringify = require('qs-stringify');

import Dataset from './dataset';
import Row from './row';

export interface IDatasetInfo {
	deleteCount: number;
	id: string;
	insertCount: number;
	lastData: number;
	name: number;
	readCount: number;
	rowCount: number;
	updateCount: number;
}

export interface IDatasetData {
	columns: Array<{
		id: number;
		linkTable: string | null;
		name: string;
	}>;
	data: Array<{
		[s: number]: any;
		dv: number;
		id: number;
	}>;
}

interface IAccessRequest {
	clientId?: string;
	clientMeta?: string;
	clientName?: string;
	conditions?: ICondition[] | IConditionSimple;
	duration?: number;
}

interface IAccessResponse {
	errors?: Array<{
		msg: string;
		name: string;
	}>;
	token: string;
}

interface ICondition {
	id?: string;
	name?: string;
	op?: '=' | '!=' | '<' | '<=' | '>=' | '>';
	set?: boolean;
	setValue?: string;
	value: string | number;
}

interface IConditionSimple {
	[key: string]: number | string;
}

interface IDatasetsResponse {
	datasets: IDatasetInfo[];
}

interface IDatasetDataResponse extends IDatasetData {}

export type IStyleFramework = 'd' | 'auto' | 'bs' | 'bs4' | 'dt' | 'zf' | 'ju' | 'se' | 'no';

export interface IOptions {
	/** Your unique identifier for the user */
	clientId?: string;

	/** Addition information that you can use to identify the user (e.g. an access type or source) */
	clientMeta?: string;

	/** Name / label to give the user in the CloudTables configuration UI */
	clientName?: string;

	/** Conditions to apply to the data to fetch */
	conditions?: ICondition[] | IConditionSimple;

	/** Domain */
	domain?: string;

	/** Authentication duration - seconds */
	duration?: number;

	/** Single role (takes priority over `roles` if both used) */
	role?: string;

	/** Array of roles (union'ed with the roles for the API key) */
	roles?: string[];

	/** Allow a self signed certificate to be accepted or not (still https) */
	secure?: boolean;

	/** Use https or not */
	ssl?: boolean;
}

export default class CloudTablesApi {
	private _subdomain: string;
	private _key: string;
	private _options: IOptions;
	private _accessKey: string | null = null;

	/**
	 * Access a SELF-HOSTED CloudTables API
	 * @param key API access key
	 * @param options Configuration options
	 */
	public constructor(key: string, options: IOptions | null); // Self hosted
	/**
	 * Access a CLOUD CloudTables API
	 * @param subdomain Sub-domain name
	 * @param key API access key
	 * @param options Configuration options
	 */
	public constructor(subdomain: string, key: string, options: IOptions | null); // Hosted
	public constructor(a: string, b: any = null, c: any = null) {
		let key: string;
		let options: IOptions | null;;

		if (typeof a === 'string' && typeof b === 'string') {
			// Cloud
			this._subdomain = a;
			key = b
			options = c;
		}
		else {
			// Self-hosted
			this._subdomain = '';
			key = a;
			options = b;
		}

		this._key = key;
		this._options = {
			clientId: undefined,
			clientMeta: undefined,
			clientName: undefined,
			conditions: undefined,
			domain: 'cloudtables.io',
			duration: undefined,
			role: undefined,
			roles: [],
			secure: true,
			...options
		};
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Public methods
	 */

	/**
	 * Get the data and columns for a given dataset
	 *
	 * @param dataset ID (UUID) of the dataset to be shown
	 * @deprecated Use `dataset().data()`
	 */
	public async data(dataset: string): Promise<IDatasetData> {
		// Note that this cannot be replaced with just the `dataset().data()` since we use the
		// /dataset/:id/data route here, while `dataset().data()` uses /dataset/:id
		let json = await this.request<IDatasetDataResponse>('/api/1/dataset/' + dataset + '/data', 'GET');

		return json;
	}

	/**
	 * Get summary information about the available datasets
	 */
	public async datasets(): Promise<IDatasetInfo[]> {
		let json = await this.request<IDatasetsResponse>('/api/1/datasets', 'GET');

		return json.datasets;
	}

	/**
	 * Manipulate a specific data set
	 * @param id Data set id
	 * @returns Data set instance
	 */
	public dataset(id: string): Dataset {
		let ds = new Dataset(this, id);

		return ds;
	}

	/**
	 * Get an access token. Note that this value is cached - a single token per instance is
	 * all that is required.
	 *
	 * @returns Access token
	 */
	public async token(): Promise<string> {
		if (this._accessKey !== null) {
			return this._accessKey;
		}

		let opts = this._options;
		let data: IAccessRequest = {};

		if (opts.duration !== undefined) {
			data.duration = opts.duration;
		}

		if (opts.clientId !== undefined) {
			data.clientId = opts.clientId;
		}

		if (opts.clientMeta !== undefined) {
			data.clientMeta = opts.clientMeta;
		}

		if (opts.clientName !== undefined) {
			data.clientName = opts.clientName;
		}

		let json = await this.request<IAccessResponse>('/api/1/access', 'POST', data);

		return json.token;
	}

	/**
	 * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
	 * where you want the table to appear in your HTML.
	 *
	 * @param token Secure access token (from `.token()`)
	 * @param datasetId ID (UUID) of the dataset to be shown
	 * @param style The styling framework to use for this table
	 * @returns `<script>` tag to use.
	 * @deprecated Use `dataset().scriptTag()`
	 */
	public scriptTag(token: string, datasetId: string, style: IStyleFramework = 'd'): string {
		return this.dataset(datasetId).scriptTag(token, style);
	}

	/**
	 * Get a `<script>` tag for a specific dataset to be displayed as a table. Insert this script tag
	 * where you want the table to appear in your HTML.
	 *
	 * This is the same as `scriptTag`, but it will retrieve the secure access token for you - thus
	 * requiring it to be async.
	 *
	 * @param datasetId ID (UUID) of the dataset to be shown
	 * @param style The styling framework to use for this table
	 * @returns `<script>` tag to use.
	 * @deprecated Use `dataset().scriptTagAsync()`
	 */
	public async scriptTagAsync(datasetId: string, style: IStyleFramework = 'd'): Promise<string> {
		return this.dataset(datasetId).scriptTagAsync(style);
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Internal methods for use by the other CT classes
	 */

	/** @internal */
	public options() {
		return this._options;
	}

	/** @internal */
	public subdomain() {
		return this._subdomain;
	}

	/**
	 * Make a request to get data
	 * @internal
	 */
	 public async request<T>(path: string, type: 'GET' | 'POST' | 'PUT' | 'DELETE', data: any = {}): Promise<T> {
		let opts = this._options;

		// Add common data
		data.key = this._key;

		if (opts.roles?.length) {
			data.roles = opts.roles;
		}

		if (opts.role) {
			data.roles = [opts.role];
		}

		if (opts.conditions !== undefined) {
			data.conditions = opts.conditions;
		}

		let postData = stringify(data as any);
		let options: https.RequestOptions = {};
		let host: string;
		
		// For self hosting, there is no subdomain and we need a /io prefix for the API server
		if (! this._subdomain) {
			host = this._options.domain!;
			path = '/io' + path;
		}
		else {
			host = this._subdomain + '.' + this._options.domain;
		}

		if (type === 'POST' || type === 'PUT') {
			options = {
				headers: {
					'Content-Length': postData.length,
					'Content-Type': 'application/x-www-form-urlencoded',
				},
				hostname: host,
				method: type,
				path,
			};
		}
		else {
			options = {
				hostname: host,
				method: type,
				path: path + '?' + postData
			};
		}

		// Allow self signed certificates
		if (! opts.secure) {
			options.rejectUnauthorized = false;
			options.agent = false;
		}

		let protocol = opts.ssl === false
			? http
			: https;

		let promise = new Promise<T>((resolve, reject) => {
			let data: Buffer[] = [];
			let req = protocol.request(options, (res: any) => {
				res.on('data', (chunk: Buffer) => {
					data.push(chunk);
				});

				res.on('end', () => {
					try {
						let str = Buffer.concat(data).toString();
						let json = JSON.parse(str);

						if (json.errors) {
							console.error('The response from the server indicated errors: ' + data);
							reject(json.errors);
						}
						else {
							resolve(json);
						}
					}
					catch (e) {
						console.error('JSON return from the server was not JSON: ' + data);
						reject('JSON encoding error');
					}
				});
			});

			req.on('error', (e: Error) => {
				reject(e);
			});

			if (type === 'POST' || type === 'PUT') {
				req.write(postData);
			}

			req.end();
		});

		return promise;
	}
}
