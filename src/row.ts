
import Dataset from './dataset';

export interface IData {
	[s: string]: any;
	dv: number;
	id: number;
}

interface IDelete {
	error?: string;
	success: boolean;
}

export default class Row {
	private _id: number;
	private _host: Dataset;

	public constructor (host: Dataset, id: number) {
		this._host = host;
		this._id = id;
	}

	public async data(): Promise<IData> {
		let json = await this._host.api().request<IData>(
			`/api/1/dataset/${this._host.id()}/${this._id}`,
			'GET'
		);

		return json;
	}

	public id() {
		return this._id;
	}

	public async update(data: {[s: string]: any}) {
		let json = await this._host.api().request<IData>(
			`/api/1/dataset/${this._host.id()}/${this._id}`,
			'PUT',
			data
		);

		return json;
	}

	public async delete() {
		let json = await this._host.api().request<IDelete>(
			`/api/1/dataset/${this._host.id()}/${this._id}`,
			'DELETE'
		);

		return json;
	}
}
