
# CloudTables API client

This library can be used by NodeJS applications to interface with the CloudTables API. At the moment the only facility provided it to request access security tokens. These tokens can then be used to securely request a CloudTable interface for a specific table, with the access rights defined by the security key and requests from this library.


## Example use:

Import the library:

```js
import CloudTablesApi from 'cloudtables-api';
// or
const CloudTablesApi = require('cloudtables-api').default;
```

Get a security token:

```js
let api = new CloudTablesApi('subdomain', 'apiKey', {
	userId: 'yourUniqueUserId',
	userName: 'User name / label'
});

let token = await api.token();
```

where:

* `subdomain` would be replaced by the sub-domain for your CloudTables application.
* `apiKey` would be replaced by your API Key (see the _Security / API Keys_ section in your CloudTables application)
* `userId` is optional, but will be used to uniquely identify user's in the CloudTables interface.
* `userName` is also optional, but can be used to help identify who made what changes when reviewing logs in CloudTables. It is recommended you include `userId` and `userName`.

Note that the `token` method returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). In the example above we have used `await` to resolve the promise - thus this would need to be executed within an `async` function, such as a suitable Express route handler or middleware. If you can't use async functions, `Promise.prototype.then()` can be used.


## Using the token

Once you have a security access token, you will want to use that as part of the script to request the CloudTables embedded table / form on your page - exactly how you get the token onto your page will depend upon what templating engine or other HTML generation you are using, but when using Handlebars you might use the following if you assign the token to a Handlebars parameter:

```html
<script src="..." data-key="{token}"></script>
```

See the _Data set / Embed_ section of your CloudTables application for more information on how to embed a CloudTables view.


## Storing the token

There is a small overhead to requesting a security access token, since it must make a request to the CloudTables' servers to validate the key and create the token. While this is unavoidable for the first request by a user, you might wish to store the access token in a session object (for example if you are using [Express Session](https://www.npmjs.com/package/express-session)), allowing the token to only be generated when the session is first setup.






